%undefine __cmake_in_source_build
%global pkg_name %{name}
%global pkgnamepatch mysql
%{!?runselftest:%global runselftest 0}
%global check_testsuite 0
%global require_mysql_selinux 1
%global _pkgdocdirname %{pkg_name}%{!?_pkgdocdir:-%{version}}
%{!?_pkgdocdir: %global _pkgdocdir %{_docdir}/%{pkg_name}-%{version}}
%global _default_patch_flags --no-backup-if-mismatch
%global skiplist platform-specific-tests.list
%bcond_without clibrary
%bcond_without devel
%bcond_without client
%bcond_without common
%bcond_without errmsg
%bcond_without test
%bcond_with config
%bcond_with debug
%global boost_bundled_version 1.77.0
%global daemon_name mysqld
%global daemon_no_prefix mysqld
%global pidfiledir %{_rundir}/%{daemon_name}
%global logrotateddir %{_sysconfdir}/logrotate.d
%global logfiledir %{_localstatedir}/log/mysql
%global logfile %{logfiledir}/%{daemon_no_prefix}.log
%global dbdatadir %{_localstatedir}/lib/mysql
%global mysqluserhome /var/lib/mysql
%bcond_without mysql_names
%bcond_without conflicts
%global sameevr   %{?epoch:%{epoch}:}%{version}-%{release}
Name:                mysql
Version:             8.0.41
Release:             1
Summary:             MySQL client programs and shared libraries
URL:                 http://www.mysql.com
License:             GPL-2.0-or-later AND LGPL-2.1-only AND BSL-1.0 AND GPL-1.0-or-later OR Artistic-1.0-Perl AND BSD-2-Clause
Source0:             https://cdn.mysql.com/Downloads/MySQL-8.0/mysql-boost-%{version}.tar.gz
Source2:             mysql_config_multilib.sh
Source3:             my.cnf.in
Source6:             README.mysql-docs
Source7:             README.mysql-license
Source10:            mysql.tmpfiles.d.in
Source11:            mysql.service.in
Source12:            mysql-prepare-db-dir.sh
Source14:            mysql-check-socket.sh
Source15:            mysql-scripts-common.sh
Source17:            mysql-wait-stop.sh
Source18:            mysql@.service.in
Source30:            %{pkgnamepatch}.rpmlintrc
Source31:            server.cnf.in
Source32:            https://cdn.mysql.com/Downloads/MySQL-8.0/mysql-boost-%{version}.tar.gz.asc
Source33:            https://repo.mysql.com/RPM-GPG-KEY-mysql-2023
Patch1:              %{pkgnamepatch}-install-test.patch
Patch3:              %{pkgnamepatch}-file-contents.patch
Patch4:              %{pkgnamepatch}-scripts.patch
Patch5:              %{pkgnamepatch}-paths.patch
Patch52:             %{pkgnamepatch}-sharedir.patch
Patch55:             %{pkgnamepatch}-rpath.patch
Patch75:             %{pkgnamepatch}-arm32-timer.patch
Patch81:             disable-moutline-atomics-for-aarch64.patch
Patch115:            boost-1.58.0-pool.patch
Patch125:            boost-1.57.0-mpl-print.patch
%ifarch riscv64
Patch128:            riscv-lock-free.patch
%endif
Patch129:            fix-protobuf-version-22-and-up.patch
Patch130:	     fix-build-error-for-loongarch64.patch

BuildRequires:       cmake gcc-c++ libaio-devel libedit-devel libevent-devel libicu-devel lz4
BuildRequires:       lz4-devel mecab-devel bison libzstd-devel libudev-devel
%ifnarch aarch64 %{arm} s390 s390x
BuildRequires:       numactl-devel
%endif
BuildRequires:       openssl openssl-devel libcurl-devel make
BuildRequires:       perl-interpreter perl-generators
BuildRequires:       rpcgen libtirpc-devel
BuildRequires:       protobuf-lite-devel rapidjson-devel zlib zlib-devel multilib-rpm-config procps
BuildRequires:       time perl(base) perl(Carp) perl(Cwd) perl(Digest::file) perl(Digest::MD5)
BuildRequires:       perl(English) perl(Env) perl(Errno) perl(Exporter) perl(Fcntl)
BuildRequires:       perl(File::Basename) perl(File::Copy) perl(File::Find) perl(File::Spec)
BuildRequires:       perl(File::Spec::Functions) perl(File::Temp) perl(FindBin) perl(Data::Dumper)
BuildRequires:       perl(Getopt::Long) perl(if) perl(IO::File) perl(IO::Handle) perl(IO::Select)
BuildRequires:       perl(IO::Socket::INET) perl(IPC::Open3) perl(JSON) perl(lib) perl(LWP::Simple)
BuildRequires:       perl(Memoize) perl(Net::Ping) perl(POSIX) perl(Socket) perl(strict)
BuildRequires:       perl(Sys::Hostname) perl(Test::More) perl(Time::HiRes) perl(Time::localtime)
BuildRequires:       perl(warnings) systemd m4 chrpath gnupg2 wget
Requires:            bash coreutils grep %{name}-common%{?_isa} = %{sameevr}
Provides:            bundled(boost) = %{boost_bundled_version}
%if %{with mysql_names}
Provides:            mysql = %{sameevr}
Provides:            mysql%{?_isa} = %{sameevr}
Provides:            mysql-compat-client = %{sameevr}
Provides:            mysql-compat-client%{?_isa} = %{sameevr}
%endif
%{?with_conflicts:Conflicts:        mariadb}
%global __requires_exclude ^perl\\((hostnames|lib::mtr|lib::v1|mtr_|My::)
%global __provides_exclude_from ^(%{_datadir}/(mysql|mysql-test)/.*|%{_libdir}/mysql/plugin/.*\\.so)$
%description
MySQL is a multi-user, multi-threaded SQL database server. MySQL is a
client/server implementation consisting of a server daemon (mysqld)
and many different client programs and libraries. The base package
contains the standard MySQL client programs and generic MySQL files.
%if %{with clibrary}

%package          libs
Summary:             The shared libraries required for MySQL clients
Requires:            %{name}-common%{?_isa} = %{sameevr}
%if %{with mysql_names}
Provides:            mysql-libs = %{sameevr}
Provides:            mysql-libs%{?_isa} = %{sameevr}
%endif
%description      libs
The mysql-libs package provides the essential shared libraries for any
MySQL client program or interface. You will need to install this package
to use any other MySQL package or any clients that need to connect to a
MySQL server.
%endif


%package          config
Summary:             The config files required by server and client
%{?with_conflicts:Conflicts:        greatsql-mysql-config}
%description      config
The package provides the config file my.cnf and my.cnf.d directory used by any
MariaDB or MySQL program. You will need to install this package to use any
other MariaDB or MySQL package if the config files are not provided in the
package itself.

%if %{with common}

%package          common
Summary:             The shared files required for MySQL server and client
Requires:            %{name}-config%{?_isa} = %{sameevr}
%description      common
The mysql-common package provides the essential shared files for any
MySQL program. You will need to install this package to use any other
MySQL package.
%endif
%if %{with errmsg}

%package          errmsg
Summary:             The error messages files required by MySQL server
Requires:            %{name}-common%{?_isa} = %{sameevr}
%description      errmsg
The package provides error messages files for the MySQL daemon
%endif

%package          server
Summary:             The MySQL server and related files
Suggests:            %{name}%{?_isa} = %{sameevr}
Requires:            mysql%{?_isa}
Requires:            %{name}-common%{?_isa} = %{sameevr} %{name}-config%{?_isa} = %{sameevr}
Requires:            %{name}-errmsg%{?_isa} = %{sameevr}
%{?mecab:Requires: mecab-ipadic}
Requires:            coreutils
Requires(pre):    /usr/sbin/useradd
Requires:            systemd
%{?systemd_requires: %systemd_requires}
Recommends:          libcap
Requires(post):   policycoreutils-python-utils
%if %require_mysql_selinux
Requires:            (mysql-selinux if selinux-policy-targeted)
%endif
%if %{with mysql_names}
Provides:            mysql-server = %{sameevr}
Provides:            mysql-server%{?_isa} = %{sameevr}
Provides:            mysql-compat-server = %{sameevr}
Provides:            mysql-compat-server%{?_isa} = %{sameevr}
%endif
%{?with_conflicts:Conflicts:        mariadb-server}
%{?with_conflicts:Conflicts:        mariadb-galera-server}
%description      server
MySQL is a multi-user, multi-threaded SQL database server. MySQL is a
client/server implementation consisting of a server daemon (mysqld)
and many different client programs and libraries. This package contains
the MySQL server and some accompanying files and directories.
%if %{with devel}

%package          devel
Summary:             Files for development of MySQL applications
%{?with_clibrary:Requires:         %{name}-libs%{?_isa} = %{sameevr}}
Requires:            openssl-devel zlib-devel libzstd-devel
%{?with_conflicts:Conflicts:        mariadb-devel}
%description      devel
MySQL is a multi-user, multi-threaded SQL database server. This
package contains the libraries and header files that are needed for
developing MySQL client applications.
%endif
%if %{with test}

%package          test
Summary:             The test suite distributed with MySQL
Requires:            %{name}%{?_isa} = %{sameevr} %{name}-common%{?_isa} = %{sameevr}
Requires:            %{name}-server%{?_isa} = %{sameevr} gzip lz4 openssl perl(Digest::file)
Requires:            perl(Digest::MD5) perl(Env) perl(Exporter) perl(Fcntl) perl(File::Temp)
Requires:            perl(FindBin) perl(Data::Dumper) perl(Getopt::Long) perl(IPC::Open3) perl(JSON)
Requires:            perl(LWP::Simple) perl(Memoize) perl(Socket) perl(Sys::Hostname)
Requires:            perl(Test::More) perl(Time::HiRes)
%{?with_conflicts:Conflicts:        mariadb-test}
%if %{with mysql_names}
Provides:            mysql-test = %{sameevr}
Provides:            mysql-test%{?_isa} = %{sameevr}
%endif
%description      test
MySQL is a multi-user, multi-threaded SQL database server. This
package contains the regression test suite distributed with
the MySQL sources.
%endif

%package             help
Summary:             Docs for development of MySQL applications.
Requires:            mysql = %{version}-%{release}
%description         help
The package provides Docs for development of MySQL applications.


%prep
# download source0 and gpg check
# wget -qO %{SOURCE0} https://user-repo.openeuler.openatom.cn/lfs-tar/mysql/mysql-boost-%{version}.tar.gz
gpg --import %{SOURCE33}
gpg --verify %{SOURCE32} %{SOURCE0}
%setup -q -n mysql-%{version}
%patch -P1 -p1
%patch -P3 -p1
%patch -P4 -p1
%patch -P5 -p1
%patch -P52 -p1
%patch -P55 -p1
%patch -P75 -p1
%patch -P81 -p1
pushd boost/boost_$(echo %{boost_bundled_version}| tr . _)
%patch -P115 -p0
%patch -P125 -p1
popd
%ifarch riscv64
%patch -P128 -p1
%endif
%patch -P129 -p1
%patch -P130 -p1
pushd mysql-test
add_test () {
    echo "$1" : BUG#0 "${@:2}" >> %{skiplist}
}
touch %{skiplist}
add_test innodb.redo_log_archive_04 failed since 8.0.17
add_test clone.remote_dml_no_binlog failed since 8.0.17
add_test auth_sec.keyring_file_data_qa sporadic since 8.0.19
add_test collations.chinese sporadic since 8.0.19
add_test main.mysql_load_data_local_dir local infile on
add_test rpl.rpl_row_jsondiff_basic_pk failed since 8.0.22
add_test rpl.rpl_row_jsondiff_basic_nokey failed since 8.0.22
%ifarch %arm aarch64
add_test gis.st_latitude
add_test gis.st_longitude
add_test perfschema.func_file_io          missing hw on arm32
add_test perfschema.func_mutex            missing hw on arm32
add_test perfschema.global_read_lock      missing hw on arm32
add_test perfschema.setup_objects         missing hw on arm32
add_test clone.remote_error_basic max_allowed_packet is 0
add_test innodb.create_tablespace
%endif
%ifarch s390x
add_test gis.geometry_class_attri_prop
add_test gis.geometry_property_function_issimple
add_test gis.gis_bugs_crashes
add_test gis.spatial_analysis_functions_buffer
add_test gis.spatial_analysis_functions_centroid
add_test gis.spatial_analysis_functions_distance
add_test gis.spatial_operators_intersection
add_test gis.spatial_op_testingfunc_mix
add_test gis.spatial_utility_function_distance_sphere
add_test gis.spatial_utility_function_simplify
add_test innodb.log_encrypt_kill main.with_recursive
add_test innodb.mysqldump_max_recordsize
add_test main.lock_multi_bug38499
add_test main.window_std_var
add_test main.window_std_var_optimized
add_test main.with_recursive
add_test x.resource_groups
add_test gis.spatial_operators_symdifference
add_test gis.spatial_operators_union
add_test main.subquery_bugs
%endif
%ifarch %arm
add_test perfschema.relaylog
%endif
popd
cp %{SOURCE2} %{SOURCE3} %{SOURCE10} %{SOURCE11} %{SOURCE12} \
   %{SOURCE14} %{SOURCE15} %{SOURCE17} %{SOURCE18} %{SOURCE31} scripts

%build
%ifarch %arm
%define _lto_cflags %{nil}
%endif
%if %runselftest
    if [ x"$(id -u)" = "x0" ]; then
        echo "mysql's regression tests fail if run as root."
        echo "If you really need to build the RPM as root, use"
        echo "--nocheck to skip the regression tests."
        exit 1
    fi
%endif
%cmake \
         -DBUILD_CONFIG=mysql_release \
         -DFEATURE_SET="community" \
         -DINSTALL_LAYOUT=RPM \
         -DDAEMON_NAME="%{daemon_name}" \
         -DDAEMON_NO_PREFIX="%{daemon_no_prefix}" \
         -DLOG_LOCATION="%{logfile}" \
         -DPID_FILE_DIR="%{pidfiledir}" \
         -DNICE_PROJECT_NAME="MySQL" \
         -DCMAKE_INSTALL_PREFIX="%{_prefix}" \
         -DSYSCONFDIR="%{_sysconfdir}" \
         -DSYSCONF2DIR="%{_sysconfdir}/my.cnf.d" \
         -DINSTALL_DOCDIR="share/doc/%{_pkgdocdirname}" \
         -DINSTALL_DOCREADMEDIR="share/doc/%{_pkgdocdirname}" \
         -DINSTALL_INCLUDEDIR=include/mysql \
         -DINSTALL_INFODIR=share/info \
         -DINSTALL_LIBEXECDIR=libexec \
         -DINSTALL_LIBDIR="%{_lib}/mysql" \
         -DRPATH_LIBDIR="%{_libdir}" \
         -DINSTALL_MANDIR=share/man \
         -DINSTALL_MYSQLSHAREDIR=share/%{pkg_name} \
         -DINSTALL_MYSQLTESTDIR=share/mysql-test \
         -DINSTALL_PLUGINDIR="%{_lib}/mysql/plugin" \
         -DINSTALL_SBINDIR=bin \
         -DINSTALL_SUPPORTFILESDIR=share/%{pkg_name} \
         -DMYSQL_DATADIR="%{dbdatadir}" \
         -DMYSQL_UNIX_ADDR="/var/lib/mysql/mysql.sock" \
         -DENABLED_LOCAL_INFILE=ON \
         -DWITH_SYSTEMD=1 \
         -DSYSTEMD_SERVICE_NAME="%{daemon_name}" \
         -DSYSTEMD_PID_DIR="%{pidfiledir}" \
         -DWITH_INNODB_MEMCACHED=ON \
         -DWITH_FIDO=bundled \
%ifnarch aarch64 %{arm} s390 s390x
         -DWITH_NUMA=ON \
%endif
%ifarch s390 s390x armv7hl
         -DUSE_LD_GOLD=OFF \
%endif
         -DWITH_ROUTER=OFF \
         -DWITH_SYSTEM_LIBS=ON \
         -DWITH_MECAB=system \
         -DWITH_BOOST=%{_vpath_srcdir}/boost \
         -DREPRODUCIBLE_BUILD=OFF \
         -DCMAKE_C_FLAGS="%{optflags}%{?with_debug: -fno-strict-overflow -Wno-unused-result -Wno-unused-function -Wno-unused-but-set-variable}" \
         -DCMAKE_CXX_FLAGS="%{optflags}%{?with_debug: -fno-strict-overflow -Wno-unused-result -Wno-unused-function -Wno-unused-but-set-variable}" \
         -DCMAKE_EXE_LINKER_FLAGS="-pie %{build_ldflags}" \
%{?with_debug: -DWITH_DEBUG=1} \
%{?with_debug: -DMYSQL_MAINTAINER_MODE=0} \
         -DTMPDIR=/var/tmp \
         -DWITH_MYSQLD_LDFLAGS="%{build_ldflags}" \
         -DCMAKE_C_LINK_FLAGS="%{build_ldflags}" \
         -DCMAKE_CXX_LINK_FLAGS="%{build_ldflags}"
%cmake -LAH

%cmake_build

%install
%cmake_install
if %multilib_capable; then
mv %{buildroot}%{_bindir}/mysql_config %{buildroot}%{_bindir}/mysql_config-%{__isa_bits}
install -p -m 0755 %{__cmake_builddir}/scripts/mysql_config_multilib %{buildroot}%{_bindir}/mysql_config
fi
install -p -m 0644 Docs/INFO_SRC %{buildroot}%{_libdir}/mysql/
install -p -m 0644 %{__cmake_builddir}/Docs/INFO_BIN %{buildroot}%{_libdir}/mysql/
mkdir -p %{buildroot}%{logfiledir}
mkdir -p %{buildroot}%{pidfiledir}
install -p -m 0755 -d %{buildroot}%{dbdatadir}
install -p -m 0750 -d %{buildroot}%{_localstatedir}/lib/mysql-files
install -p -m 0700 -d %{buildroot}%{_localstatedir}/lib/mysql-keyring

install -D -p -m 0644 %{__cmake_builddir}/scripts/my.cnf %{buildroot}%{_sysconfdir}/my.cnf

install -D -p -m 644 %{__cmake_builddir}/scripts/mysql.service %{buildroot}%{_unitdir}/%{daemon_name}.service
install -D -p -m 644 %{__cmake_builddir}/scripts/mysql@.service %{buildroot}%{_unitdir}/%{daemon_name}@.service
install -D -p -m 0644 %{__cmake_builddir}/scripts/mysql.tmpfiles.d %{buildroot}%{_tmpfilesdir}/%{daemon_name}.conf
rm -r %{buildroot}%{_tmpfilesdir}/mysql.conf
install -D -p -m 755 %{__cmake_builddir}/scripts/mysql-prepare-db-dir %{buildroot}%{_libexecdir}/mysql-prepare-db-dir
install -p -m 755 %{__cmake_builddir}/scripts/mysql-wait-stop %{buildroot}%{_libexecdir}/mysql-wait-stop
install -p -m 755 %{__cmake_builddir}/scripts/mysql-check-socket %{buildroot}%{_libexecdir}/mysql-check-socket
install -p -m 644 %{__cmake_builddir}/scripts/mysql-scripts-common %{buildroot}%{_libexecdir}/mysql-scripts-common
install -D -p -m 0644 %{__cmake_builddir}/scripts/server.cnf %{buildroot}%{_sysconfdir}/my.cnf.d/%{pkg_name}-server.cnf
rm %{buildroot}%{_libdir}/mysql/*.a
rm %{buildroot}%{_mandir}/man1/comp_err.1*
mkdir -p %{buildroot}%{logrotateddir}
mv %{buildroot}%{_datadir}/%{pkg_name}/mysql-log-rotate %{buildroot}%{logrotateddir}/%{daemon_name}
chmod 644 %{buildroot}%{logrotateddir}/%{daemon_name}
mkdir -p %{buildroot}%{_sysconfdir}/ld.so.conf.d
echo "%{_libdir}/mysql" > %{buildroot}%{_sysconfdir}/ld.so.conf.d/%{name}-%{_arch}.conf
mv %{buildroot}%{_bindir}/mysqld %{buildroot}%{_libexecdir}/mysqld
chrpath -d %{buildroot}%{_libexecdir}/mysqld
mkdir -p %{buildroot}%{_sbindir}
ln -s %{_libexecdir}/mysqld %{buildroot}%{_sbindir}/mysqld

%if %{with debug}
mv %{buildroot}%{_bindir}/mysqld-debug %{buildroot}%{_libexecdir}/mysqld
chrpath-d%{buildroot}%{_libexecdir}/mysqld
%endif

install -p -m 0644 %{SOURCE6} %{basename:%{SOURCE6}}
install -p -m 0644 %{SOURCE7} %{basename:%{SOURCE7}}
install -p -m 0644 mysql-test/%{skiplist} %{buildroot}%{_datadir}/mysql-test


mkdir -p %{buildroot}%{_sysconfdir}/my.cnf.d




%check
%if %runselftest
%ctest
cd mysql-test
cp ../../mysql-test/%{skiplist} .
export MTR_BUILD_THREAD=%{__isa_bits}
./mtr %{?with_debug:--debug-server} \
  --mem --parallel=auto --force --retry=2 \
  --mysqld=--binlog-format=mixed \
  --suite-timeout=720 --testcase-timeout=30 \
  --report-unstable-tests --clean-vardir \
%if %{check_testsuite}
  --max-test-fail=0 || :
%else
  --skip-test-list=%{skiplist}
%endif
rm -r var $(readlink var)
cd -
%endif

%pre server
/usr/sbin/groupadd -g 27 -o -r mysql >/dev/null 2>&1 || :
/usr/sbin/useradd -M -N -g mysql -o -r -d %{mysqluserhome} -s /sbin/nologin \
  -c "MySQL Server" -u 27 mysql >/dev/null 2>&1 || :

%post server
%systemd_post %{daemon_name}.service
if [ ! -e "%{logfile}" -a ! -h "%{logfile}" ] ; then
    install /dev/null -m0640 -omysql -gmysql "%{logfile}"
fi

%preun server
%systemd_preun %{daemon_name}.service

%postun server
%systemd_postun_with_restart %{daemon_name}.service

%files
%doc README README.mysql-license README.mysql-docs
%{_bindir}/{mysql,mysql_config_editor,mysqladmin,mysqlcheck,mysqldump}
%{_bindir}/{mysqlimport,mysqlpump,mysqlshow,mysqlslap,mysqlbinlog,zlib_decompress}
%exclude %{_tmpfilesdir}/mysql.conf

%files libs
%{_libdir}/mysql/libmysqlclient*.so.*
%config(noreplace) %{_sysconfdir}/ld.so.conf.d/*

%files config
%dir %{_sysconfdir}/my.cnf.d
%config(noreplace) %{_sysconfdir}/my.cnf



%files common
%dir %{_libdir}/mysql
%dir %{_datadir}/mysql
%{_datadir}/mysql/charsets

%files errmsg
%{_datadir}/mysql/messages_to_error_log.txt
%{_datadir}/mysql/messages_to_clients.txt
%{_datadir}/mysql/{errmsg-utf8.txt,english}
%lang(bg) %{_datadir}/mysql/bulgarian
%lang(cs) %{_datadir}/mysql/czech
%lang(da) %{_datadir}/mysql/danish
%lang(nl) %{_datadir}/mysql/dutch
%lang(et) %{_datadir}/mysql/estonian
%lang(fr) %{_datadir}/mysql/french
%lang(de) %{_datadir}/mysql/german
%lang(el) %{_datadir}/mysql/greek
%lang(hu) %{_datadir}/mysql/hungarian
%lang(it) %{_datadir}/mysql/italian
%lang(ja) %{_datadir}/mysql/japanese
%lang(ko) %{_datadir}/mysql/korean
%lang(no) %{_datadir}/mysql/norwegian
%lang(no) %{_datadir}/mysql/norwegian-ny
%lang(pl) %{_datadir}/mysql/polish
%lang(pt) %{_datadir}/mysql/portuguese
%lang(ro) %{_datadir}/mysql/romanian
%lang(ru) %{_datadir}/mysql/russian
%lang(sr) %{_datadir}/mysql/serbian
%lang(sk) %{_datadir}/mysql/slovak
%lang(es) %{_datadir}/mysql/spanish
%lang(sv) %{_datadir}/mysql/swedish
%lang(uk) %{_datadir}/mysql/ukrainian

%files server
%{_bindir}/{ibd2sdi,myisamchk,myisam_ftdump,myisamlog,myisampack,my_print_defaults}
%{_bindir}/{mysql_secure_installation,mysql_ssl_rsa_setup,mysql_tzinfo_to_sql,perror}
%{_bindir}/{mysql_upgrade,mysqld_pre_systemd,mysqldumpslow,innochecksum}
%{_bindir}/{mysql_keyring_encryption_test,mysql_migrate_keyring}
%config(noreplace) %{_sysconfdir}/my.cnf.d/mysql-server.cnf
%{_sbindir}/mysqld
%caps(cap_sys_nice=ep) %{_libexecdir}/mysqld
%{_libdir}/mysql/{INFO_SRC,INFO_BIN,plugin}
%dir %{_datadir}/mysql
%{_datadir}/mysql/dictionary.txt
%{_datadir}/mysql/*.sql
%{_unitdir}/mysqld*
%{_libexecdir}/{mysql-prepare-db-dir,mysql-wait-stop,mysql-check-socket,mysql-scripts-common}
%{_tmpfilesdir}/mysqld.conf
%attr(0755,mysql,mysql) %dir %{_localstatedir}/lib/mysql
%attr(0750,mysql,mysql) %dir %{_localstatedir}/lib/mysql-files
%attr(0700,mysql,mysql) %dir %{_localstatedir}/lib/mysql-keyring
%attr(0750,mysql,mysql) %dir %{_localstatedir}/log/mysql
%attr(0755,mysql,mysql) %dir %{_rundir}/mysqld
%attr(0640,mysql,mysql) %config %ghost %verify(not md5 size mtime) %{_localstatedir}/log/mysql/mysqld.log
%config(noreplace) %{_sysconfdir}/logrotate.d/mysqld

%files devel
%{_bindir}/mysql_config*
%{_includedir}/mysql
%{_datadir}/aclocal/mysql.m4
%{_libdir}/mysql/libmysqlclient.so
%{_libdir}/pkgconfig/mysqlclient.pc
%exclude %{_bindir}/mysql_config_editor


%files test
%{_bindir}/{mysql_client_test,mysqltest,mysqltest_safe_process,mysqlxtest}
%{_bindir}/{mysqld_safe,comp_err}
%attr(-,mysql,mysql) %{_datadir}/mysql-test

%files help
%doc README.mysql-license README.mysql-docs LICENSE
%doc storage/innobase/COPYING.Percona storage/innobase/COPYING.Google
%{_mandir}/man1/{mysql.1*,mysql_config_editor.1*,mysqladmin.1*,mysqlbinlog.1*,mysqldump.1*}
%{_mandir}/man1/{mysqlcheck.1*,mysqlimport.1*,mysqlpump.1*,mysqlshow.1*,mysqlslap.1*}
%{_mandir}/man1/{ibd2sdi.1*,myisamchk.1*,myisamlog.1*,myisampack.1*,myisam_ftdump.1*,mysqlman.1*}
%{_mandir}/man1/{my_print_defaults.1*,mysql_secure_installation.1*,mysql_ssl_rsa_setup.1*}
%{_mandir}/man1/{mysql_tzinfo_to_sql.1*,mysql_upgrade.1*,mysqldumpslow.1*,perror.1*}
%{_mandir}/man1/{lz4_decompress.1*,zlib_decompress.1*,innochecksum.1*,mysql.server.1*}
%{_mandir}/man8/mysqld.8*
%{_mandir}/man1/mysql_config.1*

%changelog
* Wed Jan 22 2025 yaoxin <1024769339@qq.com> - 8.0.41-1
- Update to 8.0.41 for fix CVEs(CVE-2025-21490,CVE-2025-21491,CVE-2025-21495,
  CVE-2025-21497,CVE-2025-21500,CVE-2025-21501,CVE-2025-21503,CVE-2025-21505,
  CVE-2025-21518,CVE-2025-21519,CVE-2025-21520,CVE-2025-21522,CVE-2025-21523,
  CVE-2025-21529,CVE-2025-21531,CVE-2025-21540,CVE-2025-21543,CVE-2025-21546,
  CVE-2025-21555,CVE-2025-21559)

* Thu Nov 21 2024 zhouyi <zhouyi198@h-partners.com> - 8.0.40-4
- Lfs url change

* Tue Nov 05 2024 Funda Wang <fundawang@yeah.net> - 8.0.40-3
- adopt to new cmake macro

* Tue Nov 19 2024 zhouyi <zhouyi198@h-partners.com> - 8.0.40-2
- Add Lfs config
  Edit mysql.spec

* Thu Oct 17 2024 wangkai <13474090681@163.com> - 8.0.40-1
- Update to 8.0.40 for fix CVEs(CVE-2024-21218,CVE-2024-21196,CVE-2024-21207,
  CVE-2024-21198,CVE-2024-21203,CVE-2024-21213,CVE-2024-21239,CVE-2024-21199,
  CVE-2024-21197,CVE-2024-21247,CVE-2024-21193,CVE-2024-21212,CVE-2024-21241,
  CVE-2024-21230,CVE-2024-21236,CVE-2024-21194,CVE-2024-21238,CVE-2024-21237,
  CVE-2024-21201,CVE-2024-21231,CVE-2024-21219,CVE-2024-21185)

* Mon Jul 22 2024 wangkai <13474090681@163.com> - 8.0.38-1
- Update to 8.0.38 for fix CVEs(CVE-2024-21125,CVE-2024-21142,CVE-2024-21179,
  CVE-2024-21171,CVE-2024-21130,CVE-2024-21162,CVE-2024-21177,CVE-2024-20996,
  CVE-2024-21134,CVE-2024-21165,CVE-2024-21173,CVE-2024-21129,CVE-2024-21127,
  CVE-2024-21163)

* Thu Jun 13 2024 Wenlong Zhang <zhangwenlong@loongson.cn> - 8.0.37-2
- fix build error for loongarch64

* Tue May 07 2024 wangkai <13474090681@163.com> - 8.0.37-1
- Update to 8.0.37 for fix CVEs(CVE-2024-20964,CVE-2024-20971,CVE-2024-20976,
  CVE-2024-20973,CVE-2024-20978,CVE-2024-20981,CVE-2024-20962,CVE-2024-20977,
  CVE-2024-20963,CVE-2024-20965,CVE-2024-20972,CVE-2024-20961,CVE-2024-20982,
  CVE-2024-20970,CVE-2024-20967,CVE-2024-20984,CVE-2024-20974,CVE-2024-20966,
  CVE-2024-20960,CVE-2024-20985,CVE-2024-20969,CVE-2024-21000,CVE-2024-21069,
  CVE-2024-21009,CVE-2024-21087,CVE-2024-21047,CVE-2024-20998,CVE-2024-21013,
  CVE-2024-21060,CVE-2024-21008,CVE-2024-21102,CVE-2024-21054,CVE-2024-21062,
  CVE-2024-20994,CVE-2024-21096,CVE-2024-21061,CVE-2024-20993,CVE-2024-21055,
  CVE-2024-21057,CVE-2023-6129)

* Fri Mar 22 2024 laokz <zhangkai@iscas.ac.cn> - 8.0.35-3
- Add riscv64 to fix-protobuf-version-22-and-up.patch

* Sun Feb 18 2024 Ge Wang <wang__ge@126.com> - 8.0.35-2
- Fix protobuf version 22 and up

* Tue Nov 07 2023 yaoxin <yao_xin001@hoperun.com> - 8.0.35-1
- Upgrade to 8.0.35 for fix cves

* Mon Jul 17 2023 misaka00251 <liuxin@iscas.ac.cn> - 8.0.30-4
- Import patch from Ubuntu to fix build on riscv64

* Tue May 16 2023 yaoxin <yao_xin001@hoperun.com> - 8.0.30-3
- Fix CVE-2022-37434

* Fri Mar 3 2023 Ge Wang <wangge20@h-partners.com> - 8.0.30-2
- Remove rpath

* Thu Feb 9 2023 caodongxia <caodongxia@h-partners.com> - 8.0.30-1
- Update to 8.0.30

* Thu Feb 2 2023 caodongxia <caodongxia@h-partners.com> - 8.0.29-2
- Change the compilation dependency of openssl to compat-openssl

* Mon May 9 2022 jintang song<jintang.song@epro.com.cn> - 8.0.29-1
- Upgrade mysql to 8.0.29,fix CVES:'CVE-2022-21423 CVE-2022-21451 
  CVE-2022-21444 CVE-2022-21460 CVE-2022-21417 CVE-2022-21427 CVE-2022-21414 
  CVE-2022-21435 CVE-2022-21452 CVE-2022-21413 CVE-2022-21462 CVE-2022-21412 
  CVE-2022-21415 CVE-2022-21437 CVE-2022-21438 CVE-2022-21436 CVE-2022-21418 
  CVE-2022-21459 CVE-2022-21478 CVE-2022-21479 CVE-2022-21440 CVE-2022-21425 
  CVE-2022-21457 CVE-2022-21454 CVE-2022-21483 CVE-2022-21482 CVE-2022-21484 
  CVE-2022-21485 CVE-2022-21486 CVE-2022-21489 CVE-2022-21490

* Tue Jan 25 2022 yaoxin <yaoxin30@huawei.com> - 8.0.28-1
- Upgrade mysql to 8.0.28 to fix cves.

* Fri Oct 29 2021 yaoxin <yaoxin30@huawei.com> - 8.0.27-1
- Upgrade mysql to 8.0.27,fix CVES:CVE-2021-2471 CVE-2021-2478 CVE-2021-2479
  CVE-2021-2481  CVE-2021-35546 CVE-2021-35575 CVE-2021-35577 CVE-2021-35591
  CVE-2021-35596 CVE-2021-35597 CVE-2021-35602 CVE-2021-35604 CVE-2021-35607
  CVE-2021-35608 CVE-2021-35610 CVE-2021-35612 CVE-2021-35618
  CVE-2021-35621-to-CVE-2021-35636 CVE-2021-35638-to-CVE-2021-35642
  CVE-2021-35644-to-CVE-2021-35648

* Mon Aug 2 2021 liwu <liwu13@huawei.com> - 8.0.26-1
- Upgrade mysql to 8.0.26,fix CVES:CVE-2021-2356 CVE-2021-2339 CVE-2021-2354
  CVE-2021-2352 CVE-2021-2340
 
* Wed Jun 16 2021 zhaoyao <zhaoyao32@huawei.com> - 8.0.24-2
- Fix buiding error: Built target mysqlgcs error
- Add m4 in BuildRequires

* Thu May 8 2021 wutao <wutao61@huawei.com> - 8.0.24-1
- Upgrade mysql to 8.0.24, fix CVES: CVE-2021-2166 CVE-2021-2146
  CVE-2021-2162 CVE-2021-2212 CVE-2021-2299 CVE-2021-2293 CVE-2021-2215
  CVE-2021-2278 CVE-2021-2164 CVE-2021-2208 CVE-2021-2217 CVE-2021-2203
  CVE-2021-2226 CVE-2021-2298 CVE-2021-2230 CVE-2021-2300 CVE-2021-2304
  CVE-2021-2172 CVE-2021-2194 CVE-2021-2170 CVE-2021-2196 CVE-2021-2201
  CVE-2021-2180 CVE-2021-2307 CVE-2021-2169 CVE-2021-2171 CVE-2021-2305
  CVE-2021-2179 CVE-2021-2174 CVE-2021-2193 CVE-2021-2232 CVE-2021-2301
  CVE-2021-2308

* Wed Mar 10 2021 wangxiao <wangxiao65@huawei.com> - 8.0.23-1
- Upgrade mysql to 8.0.23, fix CVES: CVE-2021-1998 CVE-2021-2002
  CVE-2021-2010 CVE-2021-2011 CVE-2021-2016 CVE-2021-2021 CVE-2021-2022
  CVE-2021-2024 CVE-2021-2031 CVE-2021-2032 CVE-2021-2036 CVE-2021-2038
  CVE-2021-2046 CVE-2021-2048 CVE-2021-2056 CVE-2021-2058 CVE-2021-2060
  CVE-2021-2061 CVE-2021-2065 CVE-2021-2070 CVE-2021-2072 CVE-2021-2076
  CVE-2021-2081 CVE-2021-2087 CVE-2021-2088 CVE-2021-2122

* Wed Jan 13 2021 Shengjing Wei <weishengjing1@huawei.com> - 8.0.22-2
- Delete useless information 

* Tue Dec 1 2020 weishengjing <weishengjing1@huawei.com> - 8.0.22-1
- New version 8.0.22 fix CVES:   CVE-2020-14852 CVE-2020-14794 CVE-2020-14775
   CVE-2020-14893 CVE-2020-14829 CVE-2020-14828 CVE-2020-14888 CVE-2020-14891
   CVE-2020-14812 CVE-2020-14870 CVE-2020-14769 CVE-2020-14878 CVE-2020-14789
   CVE-2020-14821 CVE-2020-14844 CVE-2020-14836 CVE-2020-14830 CVE-2020-14827
   CVE-2020-14773 CVE-2020-14765 CVE-2020-14869 CVE-2020-14776 CVE-2020-14861
   CVE-2020-14866 CVE-2020-14672 CVE-2020-14837 CVE-2020-14771 CVE-2020-14785
   CVE-2020-14838 CVE-2020-14848 CVE-2020-14791 CVE-2020-14793 CVE-2020-14804
   CVE-2020-14777 CVE-2020-14800 CVE-2020-14786 CVE-2020-14845 CVE-2020-14839
   CVE-2020-14846 CVE-2020-14867 CVE-2020-14868 CVE-2020-14873 CVE-2020-14860
   CVE-2020-14814 CVE-2020-14790 CVE-2020-14809

* Sat Sep 19 2020 maminjie <maminjie1@huawei.com> - 8.0.21-2
- fix internal compiler error: Segmentation fault

* Mon Sep 14 2020 maminjie <maminjie1@huawei.com> - 8.0.21-1
- upgrade to 8.0.21

* Mon Jun 1 2020 Jeffery.Gao <gaojianxing@huawei.com> - 8.0.18-2
- Package upgrade.

* Sun Mar 1 2020 zhangtao<zhangtao221@huawei.com> 8.0.17-3
- add fstack-protector-strong

* Fri Feb 28 2020 catastrowings <jianghuhao1994@163.com> 8.0.17-2
- openEuler package init.
